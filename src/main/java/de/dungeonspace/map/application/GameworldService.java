package de.dungeonspace.map.application;

import de.dungeonspace.map.controller.dto.CreateGameworldDto;
import de.dungeonspace.map.controller.dto.ResourceSettingsDto;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import de.dungeonspace.map.domain.gameworld.MapOptions;
import de.dungeonspace.map.domain.gameworld.MapType;
import de.dungeonspace.map.domain.gameworld.factory.GameworldFactoryBuilder;
import de.dungeonspace.map.domain.gameworld.factory.ResourceFactory;
import de.dungeonspace.map.domain.primitive.Util;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static de.dungeonspace.map.domain.gameworld.GameworldStatus.ACTIVE;
import static de.dungeonspace.map.domain.gameworld.GameworldStatus.INACTIVE;
import static de.dungeonspace.map.domain.gameworld.MapType.DEFAULT;

/**
 * A service responsible for managing gameworlds.
 */
@Service
@RequiredArgsConstructor
public class GameworldService {

  private final GameworldFactoryBuilder factoryBuilder;
  private final GameworldRepository gameworldRepository;
  private final ResourceFactory resourceFactory;

  private final Logger log = LoggerFactory.getLogger(GameworldService.class);

  /**
   * <p>Creates a new gameworld by choosing the appropriate factory based on the requested map type. If no map type is
   * specified a default map with a default resource distribution is created.</p>
   * <p>Disables all other gameworlds as there can only be one active.</p>
   *
   * @param config a configuration on how the gameworld should look like. Must not be null.
   * @return the gameworld id.
   */
  @Transactional
  public UUID createNewGameworld(CreateGameworldDto config) {
    assert (config != null);

    Optional<MapType> mapType = Util.getNullable(() -> config.getGameworldSettings().getMap().getType());
    Optional<Integer> mapSize = Util.getNullable(() -> config.getGameworldSettings().getMap().getSize());
    Optional<MapOptions> mapOptions = Util.getNullable(() -> config.getGameworldSettings().getMap().extractMapOptions()).flatMap(x -> x);
    Optional<ResourceSettingsDto> resourceSettings = Util.getNullable(() -> config.getGameworldSettings().getResources());

    Gameworld gameworld = factoryBuilder.chooseFactory(mapType.orElse(DEFAULT), config.getPlayerAmount(),
                    mapSize.orElse(null), mapOptions.orElse(null))
            .create();
    gameworld.changeStatus(ACTIVE);
    resourceFactory.distributeResources(gameworld, resourceSettings.orElse(null));

    gameworldRepository.findAllActive().forEach(x -> x.changeStatus(INACTIVE));
    gameworldRepository.save(gameworld);

    log.info(("New $type gameworld $id created with map grid:\n" + gameworld.getMapGrid().toString())
            .replace("$type", mapType.orElse(DEFAULT).name())
            .replace("$id", gameworld.getId().toString()));
    return gameworld.getId();
  }
}
