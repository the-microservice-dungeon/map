package de.dungeonspace.map.application;

import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import de.dungeonspace.map.domain.planet.exception.PlanetNotFoundException;
import de.dungeonspace.map.domain.planet.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * A service responsible for mining operations.
 */
@Service
@RequiredArgsConstructor
public class MineResourcesService {

  private final PlanetRepository planetRepository;
  private final Logger log = LoggerFactory.getLogger(MineResourcesService.class);

  /**
   * Attempts to mine the desired amount of resources on the given planet. If the planet does not have any resources or
   * the amount of resources left is zero a {@link ResourceNotFoundException} is thrown. If the desired amount is
   * greater than the amount left the remaining amount is mined to zero. Otherwise, the mined amount is
   * subtracted.
   *
   * @param planetId the id of the planet. Must not be null.
   * @param amountToMine the desired amount to be mined.
   * @return the amount of resources mined. Always greater than zero.
   */
  @Transactional
  public int mineResource(UUID planetId, int amountToMine) {
    assert planetId != null;

    Planet planet = planetRepository.findById(planetId).orElseThrow(() -> new PlanetNotFoundException(planetId));
    if (planet.getResource() == null) {
      throw new ResourceNotFoundException(planetId);
    }
    int initialAmount = planet.getResource().getCurrentAmount();
    int minedAmount = planet.mine(amountToMine);
    planetRepository.save(planet);

    log.info("Mined $minedAmount resources of type $resourceType of initial $initialAmount with $leftAmount left and $attemptedAmount attempted to mine."
            .replace("$minedAmount", String.valueOf(minedAmount))
            .replace("$resourceType", planet.getResource().getType().name())
            .replace("$initialAmount", String.valueOf(initialAmount))
            .replace("$leftAmount", planet.getResource().getCurrentAmount().toString())
            .replace("$attemptedAmount", String.valueOf(amountToMine)));
    return minedAmount;
  }
}
