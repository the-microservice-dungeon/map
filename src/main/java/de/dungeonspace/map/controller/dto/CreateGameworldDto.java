package de.dungeonspace.map.controller.dto;

import lombok.Data;


@Data
public class CreateGameworldDto {

    private final int playerAmount;
    private final GameworldSettingsDto gameworldSettings;
}
