package de.dungeonspace.map.controller.dto;

import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class ResourceSettingsDto {

    private final AreaSettingsDto areaSettings;
    @Min(1)
    private final Integer minAmount;
    @Min(1)
    private final Integer maxAmount;
}
