package de.dungeonspace.map.controller.dto;

import lombok.Data;


@Data
public class GameworldSettingsDto {

    private final MapSettingsDto map;
    private final ResourceSettingsDto resources;
}
