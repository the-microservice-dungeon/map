package de.dungeonspace.map.controller.dto;

public record MineResourcesDto(int amountToMine) {}
