package de.dungeonspace.map.controller.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CreateGameworldResponseDto {

    private final UUID gameworldId;
}
