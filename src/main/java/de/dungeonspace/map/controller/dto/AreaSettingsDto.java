package de.dungeonspace.map.controller.dto;

import de.dungeonspace.map.domain.planet.ResourceType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Data
@AllArgsConstructor
public class AreaSettingsDto {

    private Map<ResourceType, @Min(0) @Max(1) Double> inner;
    private Map<ResourceType, @Min(0) @Max(1) Double> middle;
    private Map<ResourceType, @Min(0) @Max(1) Double> outer;
    private Map<ResourceType, @Min(0) @Max(1) Double> border;

    public Map<ResourceType, Double> getInner() {
        return Optional.ofNullable(inner).orElse(Collections.emptyMap());
    }

    public Map<ResourceType, Double> getMiddle() {
        return Optional.ofNullable(middle).orElse(Collections.emptyMap());
    }

    public Map<ResourceType, Double> getOuter() {
        return Optional.ofNullable(outer).orElse(Collections.emptyMap());
    }

    public Map<ResourceType, Double> getBorder() {
        return Optional.ofNullable(border).orElse(Collections.emptyMap());
    }
}
