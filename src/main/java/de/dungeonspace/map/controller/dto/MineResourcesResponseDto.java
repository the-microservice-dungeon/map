package de.dungeonspace.map.controller.dto;

import lombok.Data;

@Data
public class MineResourcesResponseDto {

    private final int amountMined;
}