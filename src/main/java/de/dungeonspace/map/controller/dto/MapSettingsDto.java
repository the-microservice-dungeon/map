package de.dungeonspace.map.controller.dto;

import de.dungeonspace.map.domain.gameworld.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

@Data
@AllArgsConstructor
public class MapSettingsDto {

    @NotNull
    private final MapType type;
    @Min(5)
    private final Integer size;
    private final Map<String, String> options;

    private static final Logger log = LoggerFactory.getLogger(MapSettingsDto.class);

    /**
     * Attempts to extract the additional options based on the given map type. Does not throw in any case.
     *
     * @return an optional of the extracted options.
     */
    public Optional<MapOptions> extractMapOptions() {
        if (options == null || options.isEmpty()) {
            return Optional.empty();
        }

        return switch(type) {
            case MAZE -> parseMazeOptions(options);
            case ISLANDS -> parseIslandsOptions(options);
            case CUSTOM -> parseCustomMapOptions(options);
            case CORRIDOR -> parseCorridorOptions(options);
            case DEFAULT -> Optional.empty();
        };
    }

    private static Optional<MapOptions> parseMazeOptions(Map<String, String> options) {
        try {
            return Optional.ofNullable(options.getOrDefault(Option.CLUSTERS.label, null))
                    .map(MazeOptions::buildFromString);

        } catch (Exception e) {
            log.warn("Failed to extract maze options from request. Ignoring options.", e);
        }
        return Optional.empty();
    }

    private static Optional<MapOptions> parseIslandsOptions(Map<String, String> options) {
        try {
            String size = options.getOrDefault(Option.SIZE.label, null);
            String frequency = options.getOrDefault(Option.FREQUENCY.label, null);
            return Optional.ofNullable(IslandsOptions.buildFromStrings(size, frequency));

        } catch (Exception e) {
            log.warn("Failed to extract island options from request. Ignoring options.", e);
        }
        return Optional.empty();
    }

    private static Optional<MapOptions> parseCustomMapOptions(Map<String, String> options) {
        try {
            return Optional.ofNullable(options.getOrDefault(Option.LAYOUT.label, null))
                    .map(CustomMapOptions::new);

        } catch (Exception e) {
            log.warn("Failed to extract custom map options from request. Ignoring options.", e);
        }
        return Optional.empty();
    }

    private static Optional<MapOptions> parseCorridorOptions(Map<String, String> options) {
        try {
            String width = options.getOrDefault(Option.WIDTH.label, null);
            String frequency = options.getOrDefault(Option.FREQUENCY.label, null);
            return Optional.ofNullable(CorridorOptions.buildFromStrings(width, frequency));

        } catch (Exception e) {
            log.warn("Failed to extract corridor options from request. Ignoring options.", e);
        }
        return Optional.empty();
    }

    /**
     * Represents the supported options as label.
     */
    @ToString
    private enum Option {
        LAYOUT("layout"),
        WIDTH("width"),
        SIZE("size"),
        CLUSTERS("clusters"),
        FREQUENCY("frequency");

        private final String label;

        Option(String label) {
            this.label = label;
        }
    }
}
