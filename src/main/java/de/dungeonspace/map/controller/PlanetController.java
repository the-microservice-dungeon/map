package de.dungeonspace.map.controller;

import de.dungeonspace.map.application.MineResourcesService;
import de.dungeonspace.map.controller.dto.MineResourcesDto;
import de.dungeonspace.map.controller.dto.MineResourcesResponseDto;
import de.dungeonspace.map.domain.planet.exception.PlanetNotFoundException;
import de.dungeonspace.map.domain.planet.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * A controller for managing actions and operations on planets.
 */
@RestController
@RequestMapping("/planets")
@RequiredArgsConstructor
public class PlanetController {

  private final MineResourcesService mineResourcesService;
  private final Logger log = LoggerFactory.getLogger(PlanetController.class);

  /**
   * Mines resources on a planet and reduces the amount resources left.
   *
   * @param planetId the planet on which resources are mined.
   * @param requestBody the request body with additional information regarding the mining.
   * @return whether the mining operation was successful.
   */
  @PostMapping(value = "{planet-id}/minings", produces = "application/json", consumes = "application/json")
  public ResponseEntity<MineResourcesResponseDto> mineResource(@PathVariable("planet-id") UUID planetId,
                                                               @RequestBody MineResourcesDto requestBody) {
    log.info("Received a request to mine {} resources on planet {}.", requestBody.amountToMine(), planetId);
    int minedAmount = mineResourcesService.mineResource(planetId, requestBody.amountToMine());

    // Should be changed to 201 in the future.
    log.info("Request successful. Responding 200.");
    return ResponseEntity.ok(new MineResourcesResponseDto(minedAmount));
  }

  @ExceptionHandler
  public ResponseEntity<?> handlePlanetNotFound(PlanetNotFoundException e) {
    log.info("Request failed, planet {} not found.", e.getPlanetId());
    return ResponseEntity.of(ProblemDetail.forStatusAndDetail(
                    HttpStatusCode.valueOf(404),
                    "Planet $planetId not found.".replace("$planetId", e.getPlanetId().toString())))
            .build();
  }

  @ExceptionHandler
  public ResponseEntity<?> handleResourceNotFound(ResourceNotFoundException e) {
    log.info("Request failed with 400, no resource found on planet {}.", e.getPlanetId());
    return ResponseEntity.of(ProblemDetail.forStatusAndDetail(
                  HttpStatusCode.valueOf(400),
                  "No resources found on planet $planetId.".replace("$planetId", e.getPlanetId().toString())))
            .build();
  }
}
