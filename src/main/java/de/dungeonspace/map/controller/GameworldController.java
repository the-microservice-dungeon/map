package de.dungeonspace.map.controller;

import de.dungeonspace.map.application.GameworldService;
import de.dungeonspace.map.controller.dto.CreateGameworldDto;
import de.dungeonspace.map.controller.dto.CreateGameworldResponseDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * A controller for the management of gameworlds.<br/>
 * <br/>
 * <b>Developer Note:</b> Currently there can only be one active gameworld at most. Creating a new gameworld results in the
 * deactivation of all other gameworlds.<br/>
 * This behavior should be changed in the future by referencing gameworlds by their game-id.
 */
@RestController
@RequestMapping("/gameworlds")
@RequiredArgsConstructor
public class GameworldController {

  private final GameworldService gameworldService;
  private final Logger log = LoggerFactory.getLogger(GameworldController.class);

  /**
   * Creates a new gameworld for the given configuration. Sets the new gameworld as active and all other gameworlds as
   * inactive.<br/>
   * Tries to ignore unknown or invalid configurations when mapping the request body.
   *
   * @param requestBody game information and configurations for creating a new gameworld.
   * @return a response whether the gameworld has been created.
   */
  @PostMapping(produces = "application/json", consumes = "application/json")
  public ResponseEntity<CreateGameworldResponseDto> createGameworld(@Valid @RequestBody CreateGameworldDto requestBody) {
    log.info("Received a request to create a new gameworld for the following configuration: {}", requestBody);
    UUID gameworldId = gameworldService.createNewGameworld(requestBody);

    // Should be changed to 201 in the future.
    log.info("Request successful. Responding 200.");
    return ResponseEntity.ok().body(new CreateGameworldResponseDto(gameworldId));
  }
}
