package de.dungeonspace.map.domain.primitive;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Provides additional java utility methods.
 */
public class Util {

    /**
     * Provides a null safe way to access fields without the need to chain null checks by suppressing null pointer
     * exceptions.
     *
     * @param access the field access as a supplier
     * @return an optional of the accessed field
     * @param <A> the return type
     */
    public static <A> Optional<A> getNullable(Supplier<A> access) {
        try {
            return Optional.of(access.get());
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * Provides a null safe way to access fields without the need to chain null checks by suppressing null pointer
     * exceptions.
     *
     * @param access the field access as a supplier
     * @return an optional of the accessed field
     * @param <A> the return type
     */
    public static <A> A getNullSafe(Supplier<A> access) {
        try {
            return access.get();
        } catch (NullPointerException e) {
            return null;
        }
    }
}
