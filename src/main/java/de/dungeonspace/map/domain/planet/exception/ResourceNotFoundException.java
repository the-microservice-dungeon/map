package de.dungeonspace.map.domain.planet.exception;

import lombok.Getter;

import java.util.UUID;

/**
 * An exception raised when a resource is expected but not found on a specific planet.
 */
@Getter
public class ResourceNotFoundException extends RuntimeException {

  private final UUID planetId;

  public ResourceNotFoundException(UUID planetId) {
    this.planetId = planetId;
  }

  public ResourceNotFoundException(String message, UUID planetId) {
    super(message);
    this.planetId = planetId;
  }
}
