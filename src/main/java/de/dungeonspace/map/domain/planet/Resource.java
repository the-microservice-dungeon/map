package de.dungeonspace.map.domain.planet;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Resource {
  @Enumerated(EnumType.STRING)
  private ResourceType type;
  @Min(0)
  private Integer maxAmount;
  @Min(0)
  @Setter
  private Integer currentAmount;

  public Resource(ResourceType type, int maxAmount) {
    Objects.requireNonNull(type);

    this.type = type;
    this.maxAmount = maxAmount;
    this.currentAmount = maxAmount;
  }
}
