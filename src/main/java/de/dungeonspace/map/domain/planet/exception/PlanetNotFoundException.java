package de.dungeonspace.map.domain.planet.exception;

import lombok.Getter;

import java.util.UUID;

/**
 * An exception used for cases in which a planet is expected but not found.
 */
@Getter
public class PlanetNotFoundException extends RuntimeException {

    private final UUID planetId;

    public PlanetNotFoundException(UUID planetId) {
        super();
        this.planetId = planetId;
    }
}
