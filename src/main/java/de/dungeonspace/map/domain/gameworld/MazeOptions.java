package de.dungeonspace.map.domain.gameworld;

public record MazeOptions(
        double clusters

) implements MapOptions {

    public static MazeOptions buildFromString(String input) {
        double clusters = Double.parseDouble(input);
        if (clusters < 0 || clusters > 1) {
            throw new IllegalArgumentException("Option 'clusters' must be between 0 and 1. Is '" + input + "'.");
        }
        return new MazeOptions(clusters);
    }
}
