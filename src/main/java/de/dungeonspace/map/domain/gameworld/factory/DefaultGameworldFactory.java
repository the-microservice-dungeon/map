package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Area;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.primitive.Coordinates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DefaultGameworldFactory implements GameworldFactory {

  private final int mapSize;
  private final Random random;

  public DefaultGameworldFactory(int mapSize) {
    this.mapSize = mapSize;
    this.random = new Random();
  }

  @Override
  public Gameworld create() {
    var grid = MapGrid.create(mapSize);

    deleteRandomPlanets(grid);

    return Gameworld.create(grid);
  }

  private void deleteRandomPlanets(MapGrid grid) {
    // We just delete planets from the outer and middle area
    var coordinates = grid.getPlanetCoordinatesSorted(Area.OUTER, Area.MID);
    var amount = calculateDeleteAmount(grid);
    List<Coordinates> keys = new ArrayList<>(coordinates);
    Collections.shuffle(keys, random);

    var deletedKeys = new ArrayList<>();
    for (int i = 0; deletedKeys.size() < amount && i < keys.size(); i++) {
      var toDelete = keys.get(i);

      // We don't want to isolate planets on the grid.
      if(grid.getDirectNeighbours(toDelete).keySet().size() <= 1) {
        continue;
      }

      grid.destroyPlanet(toDelete);
      deletedKeys.add(toDelete);
    }
  }

  private int calculateDeleteAmount(MapGrid grid) {
    return grid.getPlanets().size() / 10 * random.nextInt(1, 3);
  }
}
