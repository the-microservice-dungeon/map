package de.dungeonspace.map.domain.gameworld;

import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.primitive.Coordinates;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MapGrid {
  private static final int MIN_SIZE = 5;

  @Getter
  private int size;

  // Planet does not make any sense without a gameworld, therefore we can cascade here
  @OneToMany(cascade = CascadeType.ALL)
  private Map<Coordinates, Planet> planets = new HashMap<>();

  /**
   * Creates a new map grid with the given size.
   * @param mapSize size/width of grid
   * @return new map grid
   */
  public static MapGrid create(int mapSize) {
    if(mapSize < MIN_SIZE) {
      throw new IllegalArgumentException("Map size must be at least " + MIN_SIZE);
    }

    var mapGrid = new MapGrid();
    mapGrid.size = mapSize;
    mapGrid.planets = mapGrid.createNew(mapSize);
    return mapGrid;
  }

  /**
   * Destroys a planet at the given coordinates.
   * @param coordinates The coordinates of the planet to destroy.
   */
  public void destroyPlanet(Coordinates coordinates) {
    planets.remove(coordinates);
  }

  /**
   * Patches the given planets in the map grid. Cannot be used to add new planets, only existing
   * planets will be patched.
   * @param planets the planets to patch
   */
  public void patchPlanets(Map<Coordinates, Planet> planets) {
    for (var entry : planets.entrySet()) {
      if(this.planets.containsKey(entry.getKey())) {
        this.planets.put(entry.getKey(), entry.getValue());
      }
    }
  }

  /**
   * Retrieves the whole grid.
   * @return grid as an unmodifiable map
   */
  public Map<Coordinates, Planet> getPlanets() {
    return Map.copyOf(planets);
  }

  /**
   * Retrieves a subset of the grid from the given areaSettings
   * @param area the area to retrieve
   * @return a subset of the grid
   */
  public Map<Coordinates, Planet> getPlanets(Area... area) {
    return planets.entrySet()
        .stream()
        .filter(entry -> Arrays.stream(area).anyMatch(a -> a.equals(areaOf(entry.getKey()))))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  /**
   * Gets a sorted list of coordinates, sorted by X first
   */
  public List<Coordinates> getPlanetCoordinatesSorted() {
    return planets.keySet().stream().sorted(
            Comparator
                    .comparingInt(Coordinates::getX)
                    .thenComparingInt(Coordinates::getY)
    ).toList();
  }

  /**
   * Gets a sorted list of coordinates in specific area
   */
  public List<Coordinates> getPlanetCoordinatesSorted(Area... area) {
    return getPlanets(area).keySet().stream().sorted(
            Comparator
                    .comparingInt(Coordinates::getX)
                    .thenComparingInt(Coordinates::getY)
    ).toList();
  }

  /**
   * Checks if the grid has a planet on the given coordinate
   *
   * @param coordinates the coordinate to check
   */
  public boolean hasPlanetAt(Coordinates coordinates) {
    return planets.containsKey(coordinates);
  }

  /**
   * Gets all the neighbouring coordinates of a given coordinate
   *
   * @param coordinates the coordinate to check
   * @return a list of the neighbouring coordinates
   */
  public List<Coordinates> getNeighbourCoordinates(Coordinates coordinates) {
    List<Coordinates> coordinatesList = new ArrayList<>();
    if (coordinates.getX() > 0)
      coordinatesList.add(new Coordinates(coordinates.getX() - 1, coordinates.getY()));
    if (coordinates.getY() > 0)
      coordinatesList.add(new Coordinates(coordinates.getX(), coordinates.getY() - 1));
    if (coordinates.getX() < size - 1)
      coordinatesList.add(new Coordinates(coordinates.getX() + 1, coordinates.getY()));
    if (coordinates.getY() < size - 1)
      coordinatesList.add(new Coordinates(coordinates.getX(), coordinates.getY() + 1));
    return coordinatesList;
  }

  /**
   * Analyzes the current map grid and returns all
   * groups of planets that are interconnected
   *
   * @return a list of all the interconnected coordinates
   */
  public List<List<Coordinates>> getPlanetIslands() {
    List<Coordinates> assignedCoordinates = new ArrayList<>();
    List<List<Coordinates>> islandCoordinates = new ArrayList<>();
    Set<Coordinates> planetCoordinates = planets.keySet();

    while (assignedCoordinates.size() != planetCoordinates.size()) {
      var unassignedCoordinateOptional = planetCoordinates.stream()
              .filter(it -> !assignedCoordinates.contains(it))
              .findFirst();
      if (unassignedCoordinateOptional.isEmpty()) break;

      var unassignedCoordinate = unassignedCoordinateOptional.get();
      var newlyAssignedCoordinates = getNeighboursRecursive(unassignedCoordinate);
      assignedCoordinates.addAll(newlyAssignedCoordinates);
      islandCoordinates.add(newlyAssignedCoordinates);
    }

    return islandCoordinates;
  }

  private List<Coordinates> getNeighboursRecursive(Coordinates coordinates) {
    ArrayList<Coordinates> accumulator = new ArrayList<>(){{ add(coordinates); }};
    getNeighboursRecursive(coordinates, accumulator);
    return accumulator;
  }

  private void getNeighboursRecursive(Coordinates coordinates, ArrayList<Coordinates> accumulator) {
    var neighbours = getDirectNeighbours(coordinates).keySet();
    for (var neighbour: neighbours) {
      if (!accumulator.contains(neighbour)) {
        accumulator.add(neighbour);
        getNeighboursRecursive(neighbour, accumulator);
      }
    }
  }

  public Map<Coordinates, Planet> getDirectNeighbours(Coordinates coordinates) {
    return this.planets.entrySet()
        .stream()
        .filter(entry -> entry.getKey().isAdjacent(coordinates))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  public Coordinates coordinatesOf(Planet planet) {
    return this.planets.entrySet()
        .stream()
        .filter(entry -> entry.getValue().equals(planet))
        .map(Map.Entry::getKey)
        .findFirst()
        .orElseThrow();
  }

  public Area areaOf(Coordinates coordinates) {
    var size = this.size - 1;
    var innerRadius = size / 3;
    var midRadius = innerRadius / 2;
    var x = coordinates.getX();
    var y = coordinates.getY();

    if(x == 0 || y == 0 || x == size || y == size) {
      return Area.BORDER;
    }
    if (x > innerRadius && x < size - innerRadius && y > innerRadius && y < size - innerRadius) {
      return Area.INNER;
    }
    if (x > midRadius && x < size - midRadius && y > midRadius && y < size - midRadius) {
      return Area.MID;
    }

    return Area.OUTER;
  }

  private Map<Coordinates, Planet> createNew(int size) {
    return IntStream.range(0, size)
        .mapToObj(x -> IntStream.range(0, size)
            .mapToObj(y -> new Coordinates(x, y)))
        .flatMap(x -> x)
        .collect(Collectors.toMap(c -> c, c -> Planet.create()));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int row = 0; row < size; row++) {
      for (int col = 0; col < size; col++) {
        Coordinates coordinates = new Coordinates(row, col);
        if (planets.containsKey(coordinates)) {
          sb.append("O");
        } else {
          sb.append("X");
        }
        sb.append(" ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}
