package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.IslandsOptions;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.primitive.Coordinates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class IslandsGameworldFactory implements GameworldFactory {

    private final int mapSize;
    private final IslandsOptions options;
    private final Random random;

    /**
     * Initializes the factory.
     *
     * @param mapSize the map size.
     * @param options additional options. Nullable.
     */
    public IslandsGameworldFactory(int mapSize, IslandsOptions options) {
        this.mapSize = mapSize;
        this.options = options;
        this.random = new Random();
    }

    @Override
    public Gameworld create() {
        var grid = MapGrid.create(mapSize);
        generateIslands(grid);
        return Gameworld.create(grid);
    }

    private void generateIslands(MapGrid grid) {
        double frequency = (options != null && options.frequency() != null) ? options.frequency() : 0.5;
        List<Coordinates> islands = new ArrayList<>();

        List<Coordinates> allCoordinates = new ArrayList<>(grid.getPlanetCoordinatesSorted());
        Collections.shuffle(allCoordinates, random);

        ArrayList<Coordinates> origins = new ArrayList<>();

        for (var coordinate : allCoordinates) {
            if (!islands.contains(coordinate) &&
                    grid.hasPlanetAt(coordinate) &&
                    random.nextDouble() < frequency
            ) {
                origins.add(coordinate);
                var islandCoordinates = floodFill(grid, coordinate);
                deleteIslandNeighbours(grid, islandCoordinates);
                islands.addAll(islandCoordinates);
            }
        }

        // Select the first element of the shuffled array if no islands were created, so that the map is never empty.
        if (origins.isEmpty()) {
            var origin = allCoordinates.get(0);
            origins.add(origin);
            var islandCoordinates = floodFill(grid, origin);
            deleteIslandNeighbours(grid, islandCoordinates);
            islands.addAll(islandCoordinates);
        }

        // Delete all non-island planets
        for (int row = 0; row < mapSize; row++) {
            for (int col = 0; col < mapSize; col++) {
                var coordinate = new Coordinates(row, col);
                if (!islands.contains(coordinate))
                    grid.destroyPlanet(coordinate);
            }
        }
    }

    /**
     * Deletes all neighboring planets of this island to isolate it from others.
     */
    private void deleteIslandNeighbours(MapGrid grid, List<Coordinates> coordinates) {
        var destroyedPlanets = new ArrayList<Coordinates>();
        for (var coordinate : coordinates) {
            var neighbours = grid.getNeighbourCoordinates(coordinate);
            for (var neighbour : neighbours) {
                if (!coordinates.contains(neighbour)) {
                    grid.destroyPlanet(neighbour);
                    destroyedPlanets.add(neighbour);
                }
            }
        }
    }

    private List<Coordinates> floodFill(MapGrid grid, Coordinates coordinate) {
        return floodFill(grid, coordinate, null, null);
    }

    private List<Coordinates> floodFill(MapGrid grid, Coordinates coordinate, List<Coordinates> islandCoordinates, List<Coordinates> checkedCoordinates) {
        double islandSize = (options != null && options.size() != null) ? options.size() : 0.75;
        if (islandCoordinates == null) {
            islandCoordinates = new ArrayList<>();
        }
        if (checkedCoordinates == null) {
            checkedCoordinates = new ArrayList<>();
        }
        islandCoordinates.add(coordinate);

        var neighbours = grid.getNeighbourCoordinates(coordinate);
        for (var neighbour : neighbours) {
            // Only continue with flood fill if the neighbour doesn't
            // belong to the island yet and the coordinate doesn't have a planet
            // which means it was deleted because it's bordering another island
            if (grid.hasPlanetAt(coordinate) && !islandCoordinates.contains(neighbour)) {
                // We usually don't check a coordinate for a flood fill multiple times,
                // but doing it results in islands with a nicer homogenous shape
                // (i.e. they don't have very long "arms")
                if (!checkedCoordinates.contains(neighbour) || random.nextDouble() < .15) {
                    checkedCoordinates.add(neighbour);

                    if (random.nextDouble() < islandSize) {
                        floodFill(grid, neighbour, islandCoordinates, checkedCoordinates);
                    }
                }
            }
        }

        return islandCoordinates;
    }
}
