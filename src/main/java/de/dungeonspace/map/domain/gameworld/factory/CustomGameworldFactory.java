package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.CustomMapOptions;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.gameworld.exceptions.GameworldCreationException;
import de.dungeonspace.map.domain.primitive.Coordinates;

public class CustomGameworldFactory implements GameworldFactory {

  private final int mapSize;
  private final String mapLayout;

  /**
   * Initializes the factory.
   *
   * @param mapSize the map size.
   * @param options additional options, containing a required layout.
   */
  public CustomGameworldFactory(int mapSize, CustomMapOptions options) {
    assert (options != null);
    assert (options.layout() != null);

    this.mapSize = mapSize;
    this.mapLayout = options.layout();
  }

  @Override
  public Gameworld create() {
    var grid = MapGrid.create(mapSize);
    var layout = this.mapLayout.trim();

    var currentRow = 0;
    var currentCol = 0;
    for (char coordinate: layout.toCharArray()) {
      switch (coordinate) {
        case 'X':
          grid.destroyPlanet(new Coordinates(currentCol, currentRow));
        case 'O':
          currentCol++;
          break;
        case ';':
        case '\n':
        case '\r':
          currentRow++;
          currentCol = 0;
        default:
          break;
      }
      if (currentRow > mapSize) {
        throw new GameworldCreationException("Too many rows configured.");
      }
      if (currentCol > mapSize) {
        throw new GameworldCreationException("Too many cols configured in row: " + currentRow);
      }
    }

    return Gameworld.create(grid);
  }
}
