package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.gameworld.MazeOptions;
import de.dungeonspace.map.domain.primitive.Coordinates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MazeGameworldFactory implements GameworldFactory {

    private final int mapSize;
    private final double clusterProbability;
    private final Random random;

    /**
     * Initializes the factory.
     *
     * @param mapSize the map size.
     * @param options additional options. Nullable.
     */
    public MazeGameworldFactory(int mapSize, MazeOptions options) {
        this.mapSize = mapSize;
        this.clusterProbability = (options != null) ? options.clusters() : .02;
        this.random = new Random();
    }

    @Override
    public Gameworld create() {
        var grid = MapGrid.create(mapSize);

        generateMaze(grid);

        return Gameworld.create(grid);
    }

    private void generateMaze(MapGrid grid) {
        List<Coordinates> allCoordinates = new ArrayList<>(grid.getPlanetCoordinatesSorted());
        Collections.shuffle(allCoordinates, random);

        var startingCoordinate = allCoordinates.get(0);
        List<Coordinates> mazePlanets = visit(grid, startingCoordinate);

        // Delete all planets not belonging to the maze
        for (int row = 0; row < mapSize; row++) {
            for (int col = 0; col < mapSize; col++) {
                var coordinate = new Coordinates(row, col);
                if (!mazePlanets.contains(coordinate))
                    grid.destroyPlanet(coordinate);
            }
        }
    }

    private List<Coordinates> visit (MapGrid grid, Coordinates coordinates) {
        return visit(grid, coordinates, new ArrayList<>());
    }

    private List<Coordinates> visit (MapGrid grid, Coordinates coordinates, List<Coordinates> acc) {
        acc.add(coordinates);
        var neighbours = grid.getNeighbourCoordinates(coordinates);
        Collections.shuffle(neighbours, random);
        for (var neighbour: neighbours) {
            if (acc.contains(neighbour)) continue;

            var isBorder = false;
            for (var neighboursNeighbour: grid.getNeighbourCoordinates(neighbour)) {
                if (!neighboursNeighbour.equals(coordinates) && acc.contains(neighboursNeighbour) && random.nextDouble() > clusterProbability) {
                    isBorder = true;
                    break;
                }
            }
            if (!isBorder) {
                visit(grid, neighbour, acc);
            }
        }

        return acc;
    }
}
