package de.dungeonspace.map.domain.gameworld.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "There is an error in the gameworld creation settings.")
public class GameworldCreationException extends RuntimeException {
    public GameworldCreationException(String message) {
        super(message);
    }

}
