package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.CorridorOptions;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.primitive.Coordinates;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CorridorGameworldFactory implements GameworldFactory {

    private final int mapSize;
    private final CorridorOptions options;
    private final Random random;

    /**
     * Initializes the factory.
     *
     * @param mapSize the map size.
     * @param options additional options. Nullable.
     */
    public CorridorGameworldFactory(int mapSize, CorridorOptions options) {
        this.mapSize = mapSize;
        this.options = options;
        this.random = new Random();
    }

    @Override
    public Gameworld create() {
        var grid = MapGrid.create(mapSize);

        generateCorridors(grid);

        return Gameworld.create(grid);
    }

    private void generateCorridors(MapGrid grid) {
        int corridorWidth = (options != null && options.width() != null) ? options.width() : 1;
        double corridorFrequency = (options != null && options.frequency() != null) ? options.frequency() : 0.3;

        List<Integer> corridorRows = generateCorridorIndices(mapSize, corridorFrequency, corridorWidth);
        List<Integer> corridorCols = generateCorridorIndices(mapSize, corridorFrequency, corridorWidth);

        // Mark cells as unpassable except for the corridor rows and columns
        for (int row = 0; row < mapSize; row++) {
            for (int col = 0; col < mapSize; col++) {
                boolean isCorridorRow = corridorRows.contains(row);
                boolean isCorridorCol = corridorCols.contains(col);

                if (!isCorridorRow && !isCorridorCol) {
                    grid.destroyPlanet(new Coordinates(col, row));
                }
            }
        }
    }

    private List<Integer> generateCorridorIndices(int mapSize, double corridorFrequency, int corridorWidth) {
        List<Integer> indices = new ArrayList<>();

        // Generate random indices based on the corridor frequency
        int lastCorridorIndex = -1;
        for (int i = 0; i < mapSize; i++) {
            if (random.nextDouble() < corridorFrequency) {
                if (lastCorridorIndex == -1 || i - lastCorridorIndex > corridorWidth) {
                    for (int j = 0; j < corridorWidth; j++) {
                        int index = i + j;
                        if (index < mapSize) {
                            indices.add(index);
                        }
                    }
                    lastCorridorIndex = i;
                }
            }
        }

        // If no index is chosen, select a random index to ensure at least one corridor
        if (indices.isEmpty()) {
            int randomIndex = random.nextInt(mapSize);
            indices.add(randomIndex);
        }

        return indices;
    }
}
