package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Scope("singleton")
public class GameworldFactoryBuilder {

    /**
     * Chooses the appropriate factory based on the given map type.
     *
     * @param mapType the type of map that should be created. Must not be NULL.
     * @param playerAmount the number of participating players. Must not be NULL.
     * @param mapSize the desired map size. Nullable, in which case the map size is based on the number of players.
     * @param options additional options specific for the chosen map type. Nullable.
     * @return an initialized factory based on the given settings.
     */
    public GameworldFactory chooseFactory(MapType mapType, int playerAmount, Integer mapSize, MapOptions options) {
        assert (mapType != null);

        return switch (mapType) {
            case DEFAULT -> new DefaultGameworldFactory(chooseMapSize(playerAmount, mapSize));
            case CUSTOM -> {
                assert (options != null);
                yield new CustomGameworldFactory(chooseMapSize(playerAmount, mapSize), (CustomMapOptions) options);
            }
            case ISLANDS -> {
                IslandsOptions islandsOptions = Objects.nonNull(options) ? (IslandsOptions) options : null;
                yield new IslandsGameworldFactory(chooseMapSize(playerAmount, mapSize), islandsOptions);
            }
            case MAZE -> {
                MazeOptions mazeOptions = Objects.nonNull(options) ? (MazeOptions) options : null;
                yield new MazeGameworldFactory(chooseMapSize(playerAmount, mapSize), mazeOptions);
            }
            case CORRIDOR -> {
                CorridorOptions corridorOptions = Objects.nonNull(options) ? (CorridorOptions) options : null;
                yield new CorridorGameworldFactory(chooseMapSize(playerAmount, mapSize), corridorOptions);
            }
        };
    }

    private static int chooseMapSize(int playerAmount, Integer mapSize) {
        if (mapSize != null) {
            return mapSize;
        }
        return calculateMapSize(playerAmount);
    }

    private static int calculateMapSize(int playerAmount) {
        if (playerAmount < 10) {
            return 15;
        }
        return Math.max(playerAmount, 20);
    }
}
