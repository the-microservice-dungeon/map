package de.dungeonspace.map.domain.gameworld;

import java.util.Optional;

public record IslandsOptions(
        Double size,
        Double frequency

) implements MapOptions {

    /**
     * Creates island options from nullable inputs. Can be created if at least one of both values is valid. Does not
     * raise errors in case invalid inputs are provided.
     *
     * @param inputSize the size as nullable string
     * @param inputFrequency the frequency as nullable string
     * @return IslandOptions in case one of both values is valid, otherwise NULL.
     */
    public static IslandsOptions buildFromStrings(String inputSize, String inputFrequency) {
        Optional<Double> size = parseValue(inputSize);
        Optional<Double> frequency = parseValue(inputFrequency);

        if (size.isPresent() || frequency.isPresent()) {
            return new IslandsOptions(size.orElse(null), frequency.orElse(null));
        }
        return null;
    }

    private static Optional<Double> parseValue(String input) {
        try {
            double value = Double.parseDouble(input);
            if (value < 0.0 || value > 1.0) {
                // LOG this - value provided but invalid
                return Optional.empty();
            }
            return Optional.of(value);
        } catch (Exception ignored) {
           // ignored
        }
        return Optional.empty();
    }
}
