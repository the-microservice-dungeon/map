package de.dungeonspace.map.domain.gameworld;

public enum MapType {
    DEFAULT,
    CORRIDOR,
    ISLANDS,
    MAZE,
    CUSTOM,
}
