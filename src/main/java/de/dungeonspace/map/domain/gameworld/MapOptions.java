package de.dungeonspace.map.domain.gameworld;

public sealed interface MapOptions permits CorridorOptions, CustomMapOptions, IslandsOptions, MazeOptions { }
