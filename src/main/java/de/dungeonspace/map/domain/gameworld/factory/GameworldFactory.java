package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;

public interface GameworldFactory {

  Gameworld create();
}
