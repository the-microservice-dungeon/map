package de.dungeonspace.map.domain.gameworld;

import java.util.Optional;

public record CorridorOptions(
        Integer width,
        Double frequency

) implements MapOptions {

    /**
     * Creates corridor options from nullable strings. Can be created if at least one of both values is valid. Invalid
     * values are logged and do not raise an error.
     *
     * @param inputWidth the width as nullable string.
     * @param inputFrequency the frequency as nullable string.
     * @return an CorridorOption if at least one of both values is valid, otherwise NULL.
     */
    public static CorridorOptions buildFromStrings(String inputWidth, String inputFrequency) {
        Optional<Integer> width = parseWidth(inputWidth);
        Optional<Double> frequency = parseFrequency(inputFrequency);

        if (width.isPresent() || frequency.isPresent()) {
            return new CorridorOptions(width.orElse(null), frequency.orElse(null));
        }
        return null;
    }

    private static Optional<Integer> parseWidth(String input) {
        try {
            int width = Integer.parseInt(input);
            if (width < 1) {
                // LOG this - width exists but is invalid!
                return Optional.empty();
            }
            return Optional.of(width);
        } catch (Exception ignored) {
            // ignored
        }
        return Optional.empty();
    }

    private static Optional<Double> parseFrequency(String input) {
        try {
            double frequency = Double.parseDouble(input);
            if (frequency < 1.0 || frequency > 1.0) {
                // LOG this - frequency exists but is invalid!
                return Optional.empty();
            }
            return Optional.of(frequency);
        } catch (Exception ignored) {
            // ignored
        }
        return Optional.empty();
    }
}
