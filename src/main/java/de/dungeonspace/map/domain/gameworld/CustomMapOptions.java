package de.dungeonspace.map.domain.gameworld;

public record CustomMapOptions(
        String layout

) implements MapOptions { }
