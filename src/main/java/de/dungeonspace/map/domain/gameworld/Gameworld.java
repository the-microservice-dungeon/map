package de.dungeonspace.map.domain.gameworld;

import de.dungeonspace.map.domain.gameworld.events.GameworldCreated;
import de.dungeonspace.map.domain.gameworld.events.GameworldStatusChanged;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import java.util.UUID;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
public class Gameworld extends AbstractAggregateRoot<Gameworld> {
  @Id
  private UUID id = UUID.randomUUID();

  @NotNull
  @Enumerated(EnumType.STRING)
  private GameworldStatus status = null;

  @Embedded
  private MapGrid mapGrid;

  public static Gameworld create(MapGrid mapGrid) {
    var gw = new Gameworld(mapGrid);
    gw.registerEvent(new GameworldCreated(gw));
    return gw;
  }

  private Gameworld(MapGrid mapGrid) {
    this.mapGrid = mapGrid;
  }

  public void changeStatus(GameworldStatus status) {
    this.status = status;
    this.registerEvent(new GameworldStatusChanged(this.id, this.status));
  }
}
