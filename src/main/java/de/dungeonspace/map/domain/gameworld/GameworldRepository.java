package de.dungeonspace.map.domain.gameworld;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface GameworldRepository extends JpaRepository<Gameworld, UUID> {

  @Query("select g from Gameworld g where g.status = de.dungeonspace.map.domain.gameworld.GameworldStatus.ACTIVE")
  Optional<Gameworld> findActive();

  @Query("""
    select g from Gameworld g where g.status = de.dungeonspace.map.domain.gameworld.GameworldStatus.ACTIVE
   """)
  List<Gameworld> findAllActive();

  @Query("""
    select g from Gameworld g join g.mapGrid mg join mg.planets p where p.id = ?1
   """)
  Gameworld getGameworldOfPlanet(UUID planetId);
}
