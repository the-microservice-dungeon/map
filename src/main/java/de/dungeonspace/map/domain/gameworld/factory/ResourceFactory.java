package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.controller.dto.ResourceSettingsDto;
import de.dungeonspace.map.domain.gameworld.Area;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.exceptions.GameworldCreationException;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.planet.Resource;
import de.dungeonspace.map.domain.planet.ResourceType;
import de.dungeonspace.map.domain.primitive.Coordinates;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static de.dungeonspace.map.domain.gameworld.Area.*;

@Component
@NoArgsConstructor
public class ResourceFactory {
    private static Random random;

    private static final Map<ResourceType, Double> RESOURCE_QUOTA = Map.of(
            ResourceType.COAL, 0.8,
            ResourceType.IRON, 0.5,
            ResourceType.GEM, 0.3,
            ResourceType.GOLD, 0.2,
            ResourceType.PLATIN, 0.1
    );
    private static final Map<ResourceType, Set<Area>> RESOURCE_LOCATION = Map.of(
            ResourceType.COAL, Set.of(OUTER, BORDER),
            ResourceType.IRON, Set.of(MID),
            ResourceType.GEM, Set.of(MID),
            ResourceType.GOLD, Set.of(INNER),
            ResourceType.PLATIN, Set.of(INNER)
    );

    private static final Integer DEFAULT_RESOURCE_AMOUNT = 10_000;

    public void distributeResources(Gameworld gameworld, ResourceSettingsDto resourceSettings) {
        random = new Random();

        if (resourceSettings == null) {
            distributeResourcesByDefaultSettings(gameworld, null, null);
        } else if (resourceSettings.getAreaSettings() == null) {
            distributeResourcesByDefaultSettings(gameworld, resourceSettings.getMinAmount(), resourceSettings.getMaxAmount());
        } else {
            placeArea(gameworld, BORDER, resourceSettings.getAreaSettings().getBorder(), resourceSettings.getMinAmount(), resourceSettings.getMaxAmount());
            placeArea(gameworld, OUTER, resourceSettings.getAreaSettings().getOuter(), resourceSettings.getMinAmount(), resourceSettings.getMaxAmount());
            placeArea(gameworld, MID, resourceSettings.getAreaSettings().getMiddle(), resourceSettings.getMinAmount(), resourceSettings.getMaxAmount());
            placeArea(gameworld, INNER, resourceSettings.getAreaSettings().getInner(), resourceSettings.getMinAmount(), resourceSettings.getMaxAmount());
        }
    }

    private static void placeArea(Gameworld gameworld, Area area, Map<ResourceType, Double> typeDistribution, Integer min, Integer max) {
        for (ResourceType resourceType: typeDistribution.keySet()) {
            double quotas = typeDistribution.get(resourceType);
            putResourceOnPlanetsInAreas(gameworld, resourceType, min, max, quotas, area);
        }
    }

    public static Resource createRandomResource(ResourceType resourceType, Integer minAmount, Integer maxAmount) {
        int amount = DEFAULT_RESOURCE_AMOUNT;
        if (maxAmount != null && minAmount != null) {
            if (minAmount > maxAmount) throw new GameworldCreationException("minAmount needs to be lower than maxAmount");
            amount = random.nextInt(maxAmount - minAmount + 1) + minAmount;
        } else if (minAmount != null) {
            amount = random.nextInt(DEFAULT_RESOURCE_AMOUNT - minAmount + 1) + minAmount;
        } else if (maxAmount != null) {
            amount = random.nextInt(maxAmount + 1);
        }
        return new Resource(resourceType, amount);
    }

    public static void distributeResourcesByDefaultSettings(Gameworld gameworld, Integer minAmount, Integer maxAmount) {
        var resources = ResourceType.values();

        for (ResourceType rt : resources) {
            var quota = RESOURCE_QUOTA.get(rt);
            var locations = RESOURCE_LOCATION.get(rt);

            if (quota == null || locations == null) {
                continue;
            }

            putResourceOnPlanetsInAreas(gameworld, rt, minAmount, maxAmount, quota, locations.toArray(Area[]::new));
        }
    }

    private static void putResourceOnPlanetsInAreas(Gameworld gameworld, ResourceType resourceType, Integer minAmount,
                                                    Integer maxAmount, Double quota, Area... area) {
        var mapGrid = gameworld.getMapGrid();
        var planetCoordinates = mapGrid.getPlanetCoordinatesSorted(area);
        var planets = mapGrid.getPlanets();

        var keysWithoutResources = planetCoordinates
                .stream()
                .filter(entry -> !planets.get(entry).hasResource())
                .collect(Collectors.toList());
        Collections.shuffle(keysWithoutResources, random);
        var keys = keysWithoutResources.stream().limit(
                (long) (keysWithoutResources.size() * quota)).toList();

        var planetsWithoutResources = new HashMap<Coordinates, Planet>();
        for (var key : keys) {
            var planet = planets.get(key);
            planetsWithoutResources.put(key, planet);
            planet.createResource(createRandomResource(resourceType, minAmount, maxAmount));
        }
        mapGrid.patchPlanets(planetsWithoutResources);
    }
}
