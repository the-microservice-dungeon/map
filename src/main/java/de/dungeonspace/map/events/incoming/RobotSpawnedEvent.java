package de.dungeonspace.map.events.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotSpawnedEvent(
  InnerRobot robot
) {
  public record InnerRobot(Planet planet) {}

  public record Planet(UUID planetId) {}
}
