package de.dungeonspace.map.events.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotMovedEvent(
  RobotMovement toPlanet
) {
  public record RobotMovement(UUID id) {}
}
