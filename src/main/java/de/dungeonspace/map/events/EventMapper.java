package de.dungeonspace.map.events;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.events.outgoing.FullPlanetDto;
import de.dungeonspace.map.events.outgoing.FullResourceDto;
import de.dungeonspace.map.events.outgoing.GameworldCreatedDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EventMapper {

  public GameworldCreatedDto map(Gameworld gameworld) {
    var planets = fromMapGrid(gameworld.getMapGrid());
    return new GameworldCreatedDto(gameworld.getId(), gameworld.getStatus(), planets);
  }

  private List<FullPlanetDto> fromMapGrid(MapGrid grid) {
    var planets = grid.getPlanets();
    return planets.entrySet().stream()
        .map(entry -> {
          var coords = entry.getKey();
          var planet = entry.getValue();
          var resource = planet.getResource();
          return new FullPlanetDto(
              planet.getId(),
              coords.getX(),
              coords.getY(),
              grid.areaOf(coords).getMovementDifficulty(),
              resource != null ? new FullResourceDto(
                  resource.getType(),
                  resource.getMaxAmount(),
                  resource.getCurrentAmount()
              ) : null
          );
        }).toList();
  }
}
