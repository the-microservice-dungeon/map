package de.dungeonspace.map.events;

import de.dungeonspace.map.domain.planet.event.ResourceMined;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * A kafka event publisher for planet specific events.
 */
@RequiredArgsConstructor
@Service
public class PlanetEventPublisher {

  private final EventPublisher eventPublisher;
  private final Logger log = LoggerFactory.getLogger(PlanetEventPublisher.class);

  /**
   * Listens on internal resource mined events and publishes those to kafka.
   *
   * @param event the internal resource mined event that indicates that a resource of a planet has been mined.
   */
  @EventListener
  public void handleResourceMinedEvent(ResourceMined event) {
    var message = MessageBuilder.withPayload(event)
        .setHeader("type", "ResourceMined")
        .setHeader(KafkaHeaders.TOPIC, "planet")
        .setHeader(KafkaHeaders.KEY, event.planet().toString())
        .build();
    log.info("Publishing 'ResourceMined' event in topic 'planet' with payload: " + event);
    eventPublisher.publish(message);
  }
}
