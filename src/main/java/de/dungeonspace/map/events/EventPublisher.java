package de.dungeonspace.map.events;

import org.springframework.messaging.Message;

public interface EventPublisher {
    <T> void publish(Message<T> event);
}
