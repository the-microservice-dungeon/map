package de.dungeonspace.map.events;

import de.dungeonspace.map.domain.gameworld.events.GameworldCreated;
import de.dungeonspace.map.domain.gameworld.events.GameworldStatusChanged;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * A kafka event publisher for gameworld specific events.
 */
@RequiredArgsConstructor
@Service
public class GameworldEventPublisher {

  private final EventPublisher eventPublisher;
  private final EventMapper eventMapper;
  private final Logger log = LoggerFactory.getLogger(GameworldEventPublisher.class);

  /**
   * Listens on internal gameworld created events and publishes those to kafka.
   *
   * @param event the internal gameworld created event that indicates that a new gameworld has been created.
   */
  @EventListener
  public void handleGameworldCreatedEvent(GameworldCreated event) {
    var dto = eventMapper.map(event.gameworld());
    var message = MessageBuilder
        .withPayload(dto)
        .setHeader("type", "GameworldCreated")
        .setHeader(KafkaHeaders.TOPIC, "gameworld")
        .setHeader(KafkaHeaders.KEY, event.gameworld().getId().toString())
        .build();
    log.info("Publishing 'GameworldCreated' event in topic 'gameworld' with payload: " + dto);
    eventPublisher.publish(message);
  }

  /**
   * Listens on internal gameworld status change events and publishes those to kafka.
   *
   * @param event the internal gameworld status change event that indicates that the status of a gameworld has changed.
   */
  @EventListener
  public void handleGameworldStatusEvent(GameworldStatusChanged event) {
    var message = MessageBuilder
        .withPayload(event)
        .setHeader("type", "GameworldStatusChanged")
        .setHeader(KafkaHeaders.TOPIC, "gameworld")
        .setHeader(KafkaHeaders.KEY, event.id().toString())
        .build();
    log.info("Publishing 'GameworldStatusChanged' event in topic 'gameworld' with payload: " + event);
    eventPublisher.publish(message);
  }
}
