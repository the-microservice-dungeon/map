package de.dungeonspace.map.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import de.dungeonspace.map.events.incoming.RobotMovedEvent;
import de.dungeonspace.map.events.incoming.RobotSpawnedEvent;
import de.dungeonspace.map.events.outgoing.FullResourceDto;
import de.dungeonspace.map.events.outgoing.PlanetDiscoveredDto;
import de.dungeonspace.map.events.outgoing.PlanetNeighbourDto;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

/**
 * A kafka event listener for robot specific events.
 */
@Component
@AllArgsConstructor
public class RobotEventListener {

  private final ObjectMapper objectMapper;
  private final KafkaTemplate<String, String> kafkaTemplate;
  private final PlanetRepository planetRepository;
  private final GameworldRepository gameworldRepository;
  private final Logger log = LoggerFactory.getLogger(RobotEventListener.class);

  /**
   * Listens to robot spawned and robot moved events. Extracts the planet id in both cases and publishes a planet
   * discovered event containing the neighbouring planets.
   *
   * @param event the consumed event.
   * @throws Exception throws in case the deserialization fails.
   */
  @Transactional
  @KafkaListener(topics = "robot")
  public void handleRobotMovedEvent(ConsumerRecord<String, String> event) throws Exception {
    String type = extractEventType(event);

    switch (type) {
      case "RobotSpawned" -> processRobotSpawnedEvent(event);
      case "RobotMoved" -> processRobotMovedEvent(event);
      default -> {}
    }
  }

  private void processRobotSpawnedEvent(ConsumerRecord<String, String> inputEvent) throws JsonProcessingException {
    RobotSpawnedEvent event = objectMapper.readValue(inputEvent.value(), RobotSpawnedEvent.class);
    UUID planetId = event.robot().planet().planetId();

    log.info("Received 'RobotSpawned' event: {}", event);
    publishPlanetDiscoveredEvent(planetId, extractPlayerId(inputEvent));
  }

  private void processRobotMovedEvent(ConsumerRecord<String, String> inputEvent) throws JsonProcessingException {
    RobotMovedEvent event = objectMapper.readValue(inputEvent.value(), RobotMovedEvent.class);
    UUID planetId = event.toPlanet().id();

    log.info("Received 'RobotMoved' event: {}", event);
    publishPlanetDiscoveredEvent(planetId, extractPlayerId(inputEvent));
  }

  // Developer Note: Should be refactored into multiple functions and extracted into a separate publishing class.
  // Requires good testing as well.
  private void publishPlanetDiscoveredEvent(UUID planetId, Collection<UUID> playerIds) {
    var planet = planetRepository.findById(planetId).orElseThrow();
    var gw = gameworldRepository.getGameworldOfPlanet(planet.getId());
    var grid = gw.getMapGrid();
    var coordinatesOfPlanet = grid.coordinatesOf(planet);
    var neighbours = grid.getDirectNeighbours(coordinatesOfPlanet);

    var neighbourDtos = neighbours.entrySet().stream()
        .map(entry -> new PlanetNeighbourDto(entry.getValue().getId(), coordinatesOfPlanet.getRelativeDirectionTo(entry.getKey())))
        .toList();
    var resource = planet.getResource();
    var planetDiscoveredEvent = new PlanetDiscoveredDto(
        planet.getId(),
        grid.areaOf(coordinatesOfPlanet).getMovementDifficulty(),
        neighbourDtos,
        resource != null ? new FullResourceDto(resource.getType(), resource.getMaxAmount(), resource.getCurrentAmount()) : null
    );

    String planetDiscoveredEventJson = null;
    try {
      planetDiscoveredEventJson = objectMapper.writeValueAsString(planetDiscoveredEvent);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
    List<Header> headers = new ArrayList<>();
    playerIds.forEach(playerId -> headers.add(new RecordHeader("playerId", playerId.toString().getBytes())));
    headers.add(new RecordHeader("type", "PlanetDiscovered".getBytes()));
    headers.add(new RecordHeader("timestamp", Instant.now().toString().getBytes()));
    var record = new ProducerRecord<>("planet", null, planet.getId().toString(), planetDiscoveredEventJson, headers);
    log.info("Publishing 'PlanetDiscovered' event in topic 'planet' with payload: " + planetDiscoveredEventJson);
    kafkaTemplate.send(record);
  }

  private static String extractEventType(ConsumerRecord<String, String> event) {
    return new String(event.headers().lastHeader("type").value());
  }

  private static List<UUID> extractPlayerId(ConsumerRecord<String, String> event) {
    return StreamSupport.stream(event.headers().headers("playerId").spliterator(), false)
            .map(header -> UUID.fromString(new String(header.value())))
            .toList();
  }
}
