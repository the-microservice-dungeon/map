package de.dungeonspace.map.events.outgoing;

import java.util.List;
import java.util.UUID;

public record PlanetDiscoveredDto(
    UUID planet,
    Integer movementDifficulty,
    List<PlanetNeighbourDto> neighbours,
    FullResourceDto resource
) { }
