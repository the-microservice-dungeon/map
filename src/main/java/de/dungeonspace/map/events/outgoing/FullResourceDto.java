package de.dungeonspace.map.events.outgoing;

import de.dungeonspace.map.domain.planet.ResourceType;

public record FullResourceDto(
    ResourceType resourceType,
    int maxAmount,
    int currentAmount
) { }
