package de.dungeonspace.map.events.outgoing;

import de.dungeonspace.map.domain.primitive.Direction;

import java.util.UUID;

public record PlanetNeighbourDto(
    UUID id,
    Direction direction
) { }
