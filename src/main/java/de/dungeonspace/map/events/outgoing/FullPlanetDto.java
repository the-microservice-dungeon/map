package de.dungeonspace.map.events.outgoing;

import java.util.UUID;

public record FullPlanetDto(
    UUID id,
    int x,
    int y,
    int movementDifficulty,
    FullResourceDto resource
) { }
