package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.CorridorOptions;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.primitive.Coordinates;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CorridorGameworldFactoryTest {

  @Test
  void shouldCreateSimpleGameworld() {
    var gameworldSimple = createTestGameworld(7, 1, 1.);
    assertNotNull(gameworldSimple);

    var grid = gameworldSimple.getMapGrid();
    assertThat(grid.getSize()).isEqualTo(7);

    // Since in this gameworld the "frequency" option is set to 1,
    // we expect the gameworld to have a "chessboard" like structure,
    // since there always has to be an empty row after a corridor
    assertThat(grid.toString()).isEqualTo(
            """
                    O O O O O O O\s
                    O X O X O X O\s
                    O O O O O O O\s
                    O X O X O X O\s
                    O O O O O O O\s
                    O X O X O X O\s
                    O O O O O O O\s
                    """
    );
  }

  @Test
  void shouldCreateComplexGameworld() {
    var gameworldSimple = createTestGameworld(50, 3, 0.2);
    assertNotNull(gameworldSimple);

    var grid = gameworldSimple.getMapGrid();
    assertThat(grid.getSize()).isEqualTo(50);
    var planets = grid.getPlanets();

    // We have to find the first non-corridor rows and cols to check
    // if the width was correctly applied.
    var firstNonCorridorXCoord = 0;
    var firstNonCorridorYCoord = 0;
    for (int i = 0; i < 50; i++) {
      if (!planets.containsKey(new Coordinates(i, 0))) {
        firstNonCorridorXCoord = i;
        break;
      }
    }
    for (int i = 0; i < 50; i++) {
      if (!planets.containsKey(new Coordinates(0, i))) {
        firstNonCorridorYCoord = i;
        break;
      }
    }

    for (var coordinateType: List.of("x", "y")) {
      var currentCorridorWidth = 0;
      var hasFullWidthCorridor = false;
      for (int i = 0; i < grid.getSize(); i++) {
        var coordinate = new Coordinates(
                coordinateType.equals("x") ? i : firstNonCorridorXCoord,
                coordinateType.equals("y") ? i : firstNonCorridorYCoord
        );
        if (planets.containsKey(coordinate)) {
          currentCorridorWidth++;
        } else {
          currentCorridorWidth = 0;
        }
        if (currentCorridorWidth == 3) {
          hasFullWidthCorridor = true;
        }
        assertThat(currentCorridorWidth).isLessThanOrEqualTo(3);
      }
      assertTrue(hasFullWidthCorridor);
    }
  }

  private Gameworld createTestGameworld(Integer mapSize, Integer width, Double frequency) {
    CorridorOptions options = new CorridorOptions(width, frequency);
    return new CorridorGameworldFactory(mapSize, options).create();
  }
}
