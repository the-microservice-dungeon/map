package de.dungeonspace.map.domain.gameworld.factory;

class CustomGameworldFactoryTest {

//  public static String testLayout = "XOOXX;OOOOO;OXXOO;XXXXO;OOOOO";
//
//  @Test
//  void shouldCreateGameworld() {
//    var gameworld = createTestGameworld();
//    assertNotNull(gameworld);
//
//    var grid = gameworld.getMapGrid();
//    assertThat(grid.getSize()).isEqualTo(5);
//
//    var planets = grid.getPlanets();
//    // Check that the planet count is correct
//    assertThat(planets.size())
//            .isEqualTo(16);
//
//    // Check some sample planets to see if they are on the right coordinates
//    assertThat(planets.get(new Coordinates(0, 0))).isNull();
//    assertThat(planets.get(new Coordinates(2, 1))).isNotNull();
//    assertThat(planets.get(new Coordinates(3, 3))).isNull();
//    assertThat(planets.get(new Coordinates(4, 4))).isNotNull();
//
//    var resourceOptions = new HashMap<Area, Map<ResourceType, Double>>();
//    resourceOptions.put(Area.BORDER, Map.of(ResourceType.COAL, 1.));
//
//    var resourceSettings = new ResourceSettingsDto(resourceOptions, 1000, 5000);
//    ResourceFactory.distributeResources(gameworld, resourceSettings, null);
//
//    // Check that EVERY border planets has resources, but no other
//    var borderPlanets = grid.getPlanets(Area.BORDER).values();
//    assertThat(borderPlanets)
//        .filteredOn(planet -> planet.hasResource() &&
//                planet.getResource().getMaxAmount() >= 1000 &&
//                planet.getResource().getMaxAmount() <= 5000
//        )
//        .hasSize(borderPlanets.size());
//
//    var centerPlanets = grid.getPlanets(Area.OUTER, Area.MID, Area.INNER).values();
//    assertThat(centerPlanets)
//            .filteredOn(Planet::hasResource)
//            .hasSize(0);
//
//  }
//
//  @Test
//  void shouldCreateGameworldWithSeededResources() {
//    var gameworld = createTestGameworld();
//
//    var grid = gameworld.getMapGrid();
//    var planets = grid.getPlanets();
//
//    var resourceOptions = new HashMap<Area, Map<ResourceType, Double>>();
//    resourceOptions.put(Area.BORDER, Map.of(ResourceType.COAL, 1.));
//
//    var resourceSettings = new ResourceSettingsDto(resourceOptions, 1000, 5000);
//    ResourceFactory.distributeResources(gameworld, resourceSettings, testSeed);
//
//    // Check that the seed worked by checking some of the values
//    assertThat(planets.get(new Coordinates(1,0)).getResource().getMaxAmount())
//            .isEqualTo(1771);
//    assertThat(planets.get(new Coordinates(4,2)).getResource().getMaxAmount())
//            .isEqualTo(4599);
//    assertThat(planets.get(new Coordinates(3,4)).getResource().getMaxAmount())
//            .isEqualTo(4850);
//  }
//
//  private Gameworld createTestGameworld() {
//    CustomMapOptions options = new CustomMapOptions(testLayout);
//    return new CustomGameworldFactory(5, options).create();
//  }
}
