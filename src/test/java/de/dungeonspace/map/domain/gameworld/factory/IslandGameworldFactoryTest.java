package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.IslandsOptions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class IslandGameworldFactoryTest {

    @Test
    void shouldCreateSimpleGameworld() {
        var gameworldSimple = createTestGameworld(30, 0.6, 0.2);
        assertNotNull(gameworldSimple);

        var grid = gameworldSimple.getMapGrid();

        assertThat(grid.getPlanets().size()).isGreaterThan(100);
        assertThat(grid.getPlanets().size()).isLessThan(700);
        assertThat(grid.getPlanetIslands().size()).isGreaterThan(10);
    }

    @Test
    void gameworldNotEmpty() {
        var gameworldSimple = createTestGameworld(5, 0.2, 1.);
        assertNotNull(gameworldSimple);

        var grid = gameworldSimple.getMapGrid();

        assertThat(grid.getPlanets().size()).isGreaterThan(0);
    }

    private Gameworld createTestGameworld(Integer mapSize, Double size, Double frequency) {
        IslandsOptions options = new IslandsOptions(size, frequency);
        return new IslandsGameworldFactory(mapSize, options).create();
    }
}
