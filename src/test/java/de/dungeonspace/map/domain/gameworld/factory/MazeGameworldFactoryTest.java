package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MazeOptions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MazeGameworldFactoryTest {

    @Test
    void shouldCreateMaze() {
        var gameworldSimple = createTestGameworld(10, .0);
        assertNotNull(gameworldSimple);

        var grid = gameworldSimple.getMapGrid();
        assertThat(grid.getPlanetIslands()).hasSize(1);
    }

    private Gameworld createTestGameworld(Integer mapSize, Double clusters) {
        MazeOptions options = new MazeOptions(clusters);
        return new MazeGameworldFactory(mapSize, options).create();
    }
}
