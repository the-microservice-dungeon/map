package de.dungeonspace.map.controller;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.application.GameworldService;
import de.dungeonspace.map.controller.dto.*;
import de.dungeonspace.map.domain.gameworld.MapType;
import de.dungeonspace.map.domain.planet.ResourceType;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class GameworldControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private GameworldService gameworldServiceMock;
  private static final ObjectMapper mapper = new ObjectMapper().registerModules();

  @ParameterizedTest
  @MethodSource("createGameworldDataProvider")
  public void testCreateEmptyGameworld(String description, String request, CreateGameworldDto expected) throws Exception {
    // given
    UUID responseGameworldId = UUID.randomUUID();
    String expectedResponse = mapper.writeValueAsString(new CreateGameworldResponseDto(responseGameworldId));
    doReturn(responseGameworldId).when(gameworldServiceMock).createNewGameworld(any());

    // when
    mockMvc.perform(post("/gameworlds")
                    .accept("application/json")
                    .contentType("application/json")
                    .content(request))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))
            .andExpect(content().json(expectedResponse));

    // then
    verify(gameworldServiceMock).createNewGameworld(expected);
  }

  private static Stream<Arguments> createGameworldDataProvider() {
    return Stream.of(
            createGameworldCase1("DEFAULT w/ gameworld null"),
            createGameworldCase2("DEFAULT w/ gameworld minimal"),
            createGameworldCase3("CORRIDOR w/ options w/ resource settings"));
  }

  private static Arguments createGameworldCase1(String description) {
    String request = """
            {
              "playerAmount": 0,
              "gameworldSettings": null,
              "unknown": true
            }""";
    CreateGameworldDto expected = new CreateGameworldDto(0, null);

    return Arguments.of(description, request, expected);
  }

  private static Arguments createGameworldCase2(String description) {
    String request = """
            {
              "playerAmount": 0,
              "gameworldSettings": {
                "map": {
                  "type": "DEFAULT"
                }
              }
            }""";
    CreateGameworldDto expected = new CreateGameworldDto(0, new GameworldSettingsDto(
            new MapSettingsDto(MapType.DEFAULT, null, null), null));

    return Arguments.of(description, request, expected);
  }

  private static Arguments createGameworldCase3(String description) {
    String request = """
            {
              "playerAmount": 1,
              "gameworldSettings": {
                "map": {
                  "type": "CORRIDOR",
                  "size": null,
                  "options": {
                    "width": 2,
                    "frequency": 2
                  }
                },
                "resources": {
                  "minAmount": 15,
                  "maxAmount": 35,
                  "areaSettings": {
                    "inner": {
                      "COAL": 0.3
                    }
                  }
                }
              }
            }""";
    CreateGameworldDto expected = new CreateGameworldDto(1, new GameworldSettingsDto(
            new MapSettingsDto(MapType.CORRIDOR, null, Map.of("width", "2", "frequency", "2")),
            new ResourceSettingsDto(new AreaSettingsDto(Map.of(ResourceType.COAL, 0.3), null, null, null), 15, 35)));

    return Arguments.of(description, request, expected);
  }
}
