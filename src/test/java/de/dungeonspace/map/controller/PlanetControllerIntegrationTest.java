package de.dungeonspace.map.controller;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.application.MineResourcesService;
import de.dungeonspace.map.controller.dto.MineResourcesResponseDto;
import de.dungeonspace.map.domain.planet.exception.PlanetNotFoundException;
import de.dungeonspace.map.domain.planet.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class PlanetControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private MineResourcesService mineResourcesServiceMock;
  private static final ObjectMapper mapper = new ObjectMapper().registerModules();

  @ParameterizedTest
  @MethodSource("mineResourcesDataProvider")
  public void testMineResources(String description, UUID planetId, String request, int expectedAmount) throws Exception {
    // given
    String expectedResponse = mapper.writeValueAsString(new MineResourcesResponseDto(expectedAmount));
    doReturn(expectedAmount).when(mineResourcesServiceMock).mineResource(any(UUID.class), anyInt());

    // when
    mockMvc.perform(post("/planets/{planet-id}/minings", planetId)
                    .accept("application/json")
                    .contentType("application/json")
                    .content(request))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))
            .andExpect(content().json(expectedResponse));

    // then
    verify(mineResourcesServiceMock).mineResource(planetId, expectedAmount);
  }

  private static Stream<Arguments> mineResourcesDataProvider() {
    return Stream.of(mineResourcesCase1("SUCCESSFUL request w/ amount of 50"));
  }

  private static Arguments mineResourcesCase1(String description) {
    return Arguments.of(description, UUID.randomUUID(), buildRequestBody(50), 50);
  }

  @Test
  public void testMineResourcesWithPlanetNotFound() throws Exception {
    // given
    UUID planetId = UUID.randomUUID();
    doThrow(new PlanetNotFoundException(planetId)).when(mineResourcesServiceMock).mineResource(any(UUID.class), anyInt());

    // when, then
    mockMvc.perform(post("/planets/{planet-id}/minings", planetId)
                    .accept("application/json")
                    .contentType("application/json")
                    .content(buildRequestBody(50))
            )
            .andDo(print())
            .andExpect(status().isNotFound());
  }

  @Test
  public void testMineResourcesWithResourceNotFound() throws Exception {
    // given
    UUID planetId = UUID.randomUUID();
    doThrow(new ResourceNotFoundException(planetId)).when(mineResourcesServiceMock).mineResource(any(UUID.class), anyInt());

    // when, then
    mockMvc.perform(post("/planets/{planet-id}/minings", planetId)
                    .accept("application/json")
                    .contentType("application/json")
                    .content(buildRequestBody(50))
            )
            .andDo(print())
            .andExpect(status().isBadRequest());
  }

  private static String buildRequestBody(int amount) {
    return """
            {
              "amountToMine": $amount
            }""".replace("$amount", String.valueOf(amount));
  }
}
