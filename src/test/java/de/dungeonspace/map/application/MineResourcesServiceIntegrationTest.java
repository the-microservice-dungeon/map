package de.dungeonspace.map.application;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import de.dungeonspace.map.domain.planet.Resource;
import de.dungeonspace.map.domain.planet.ResourceType;
import de.dungeonspace.map.domain.planet.exception.PlanetNotFoundException;
import de.dungeonspace.map.domain.planet.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

@RecordApplicationEvents
@Transactional
class MineResourcesServiceIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private MineResourcesService mineResourcesService;
  @MockBean
  private PlanetRepository planetRepositoryMock;

  @Test
  public void testMineResource() {
    // given
    Planet planet = spy(new Planet(UUID.randomUUID(), new Resource(ResourceType.PLATIN, 3000)));
    when(planetRepositoryMock.findById(Mockito.any())).thenReturn(Optional.of(planet));

    // when
    int actualMined = mineResourcesService.mineResource(planet.getId(), 100);

    // then
    assertThat(actualMined).isEqualTo(100);

    verify(planetRepositoryMock).findById(planet.getId());
    verify(planetRepositoryMock).save(planet);
    verify(planet).mine(100);
  }

  @Test
  public void testMineResourceWithoutResourceOnPlanet() {
    // given
    Planet planet = new Planet(UUID.randomUUID(), null);
    when(planetRepositoryMock.findById(any())).thenReturn(Optional.of(planet));

    // when then
    assertThrows(ResourceNotFoundException.class, () -> {
      mineResourcesService.mineResource(planet.getId(), 100);
    });
  }

  @Test
  public void testMineResourceWithPlanetNotFound() {
    // when then
    assertThrows(PlanetNotFoundException.class, () -> mineResourcesService.mineResource(UUID.randomUUID(), 100));
  }
}
