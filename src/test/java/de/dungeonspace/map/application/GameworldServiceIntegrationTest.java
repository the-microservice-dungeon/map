package de.dungeonspace.map.application;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.controller.dto.CreateGameworldDto;
import de.dungeonspace.map.controller.dto.GameworldSettingsDto;
import de.dungeonspace.map.controller.dto.MapSettingsDto;
import de.dungeonspace.map.domain.gameworld.*;
import de.dungeonspace.map.domain.gameworld.factory.DefaultGameworldFactory;
import de.dungeonspace.map.domain.gameworld.factory.GameworldFactoryBuilder;
import de.dungeonspace.map.domain.gameworld.factory.ResourceFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RecordApplicationEvents
@Transactional
class GameworldServiceIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private GameworldService gameworldService;
  @MockBean
  private GameworldRepository gameworldRepositoryMock;
  @MockBean
  private GameworldFactoryBuilder factoryBuilderMock;
  @MockBean
  private DefaultGameworldFactory factoryMock;
  @MockBean
  private ResourceFactory resourceFactoryMock;

  @Test
  public void testCreateNewGameworld() {
    // given
    CreateGameworldDto inputConfig = new CreateGameworldDto(5, new GameworldSettingsDto(
            new MapSettingsDto(MapType.DEFAULT, 3, Map.of("a", "b")), null));
    UUID gameworldId = UUID.randomUUID();

    Gameworld newGameworldMock = spy(new Gameworld(gameworldId, GameworldStatus.INACTIVE, MapGrid.create(5)));
    Gameworld activeGameworldMock = spy(new Gameworld(UUID.randomUUID(), GameworldStatus.ACTIVE, null));

    when(factoryMock.create()).thenReturn(newGameworldMock);
    when(factoryBuilderMock.chooseFactory(any(), anyInt(), any(), any())).thenReturn(factoryMock);
    when(gameworldRepositoryMock.findAllActive()).thenReturn(List.of(activeGameworldMock));

    // when
    UUID actualId = gameworldService.createNewGameworld(inputConfig);

    // then
    assertThat(actualId).isEqualTo(gameworldId);

    verify(factoryBuilderMock).chooseFactory(MapType.DEFAULT, 5, 3, null);
    verify(resourceFactoryMock).distributeResources(newGameworldMock, null);
    verify(newGameworldMock).changeStatus(GameworldStatus.ACTIVE);
    verify(activeGameworldMock).changeStatus(GameworldStatus.INACTIVE);
    verify(gameworldRepositoryMock).save(newGameworldMock);
  }
}
