include:
  #  - template: Security/Container-Scanning.gitlab-ci.yml
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file:
      - "helm/package-publish.yaml"

stages:
  - helm
  - test
  - containerize
  - scan
  - deploy

# not needed with kaniko kubernetes runner
#services:
#  - name: docker:dind
#    command: ["--tls=false"]

variables:
  # This forces GitLab to only clone the latest commit of the current branch when running the pipeline.
  # This improves speed and reliability because it limits the amount of stuff that needs to be cloned on every run.
  GIT_DEPTH: 1
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  # Set Image reference path globally to avoid duplicates
  # ${CI_REGISTRY_IMAGE} is set to own repository
  IMAGE_REF: registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/map
  # The directory of the Helm Chart
  PATH_TO_CHART: "helm-chart"
  # The name of our Chart
  CHART_NAME: "map"

  # not use on kaniko kubernetes runner
  # DinD config
  #DOCKER_HOST: "tcp://docker:2375"
  #DOCKER_TLS_CERTDIR: ""
  #DOCKER_DRIVER: overlay2


# run unit tests (also on merge requests)
test:
  stage: test


  # This template uses jdk17 for verifying and deploying images
  image: maven:3.8.5-eclipse-temurin-17

  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    paths:
      - .m2/repository

  script:
    - mvn $MAVEN_CLI_OPTS test

# containerize stage
containerize:
  image: maven:3.8.5-eclipse-temurin-17
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    paths:
      - .m2/repository
  needs:
    - test
  stage: containerize

  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

  script:
    - >
      args=(
        "-Djib.to.image=${IMAGE_REF}"
        "-Djib.to.tags=${CI_COMMIT_BRANCH}"
        "-Djib.from.image=eclipse-temurin:17-jdk"
        "-Djib.from.platforms=linux/amd64,linux/arm64"
        "-Djib.to.auth.username=${CI_CONTAINER_USER}"
        "-Djib.to.auth.password=${CI_CONTAINER_PASSWORD}"
      )
    - >
      # Deactivated because of a possible regression with GitLab container registry: https://gitlab.com/gitlab-org/container-registry/-/issues/993
      # if [ -n "${CI_COMMIT_TAG}" ];
      # then
      #   args+=( "-Djib.to.tags=${CI_COMMIT_TAG},${CI_COMMIT_SHORT_SHA}" );
      # elif [ "${CI_COMMIT_BRANCH}" == "${CI_DEFAULT_BRANCH}" ];
      # then
      #   args+=( "-Djib.to.tags=${CI_COMMIT_SHORT_SHA}" ); 
      # fi
    - mvn compile com.google.cloud.tools:jib-maven-plugin:build "${args[@]}"



helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always

